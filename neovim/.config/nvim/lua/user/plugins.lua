-- Automatically install packer
local install_path = vim.fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
    PACKER_BOOTSTRAP = vim.fn.system {
        "git",
        "clone",
        "--depth",
        "1",
        "https://github.com/wbthomason/packer.nvim",
        install_path,
    }
    print "Installing packer close and reopen Neovim..."
    vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end

-- Have packer use a popup window
packer.init {
    display = {
        open_fn = function()
            return require("packer.util").float { border = "rounded" }
        end,
    },
}

return packer.startup(function(use)
    use "wbthomason/packer.nvim"
    use "nvim-lua/plenary.nvim"
    use "kyazdani42/nvim-web-devicons"
    use {
        "EdenEast/nightfox.nvim",
        config = function()
            require "user.conf.nightfox"
        end,
    }
    use {
        "nvim-treesitter/nvim-treesitter",
        config = function()
            require "user.conf.treesitter"
        end,
        event = { "BufRead", "BufNewFile" },
        run = ":TSUpdate",
    }
    use {
        "nvim-lualine/lualine.nvim",
        after = "nvim-web-devicons",
        config = function()
            require "user.conf.lualine"
        end,
    }
    use {
        "akinsho/bufferline.nvim",
        after = "nvim-web-devicons",
        config = function()
            require "user.conf.bufferline"
        end,
    }
    use {
        "lukas-reineke/indent-blankline.nvim",
        after = "nvim-treesitter",
        config = function()
            require "user.conf.indent-blankline"
        end,
    }
    use {
        "lewis6991/gitsigns.nvim",
        config = function()
            require "gitsigns".setup()
        end,
    }
    use {
        "numToStr/Comment.nvim",
        config = function()
            require "Comment".setup()
        end,
    }
    use {
        "folke/which-key.nvim",
        config = function()
            require "which-key".setup()
        end,
    }
    use {
        "kyazdani42/nvim-tree.lua",
        config = function()
            require "nvim-tree".setup()
        end
    }
    use {
        "nvim-telescope/telescope.nvim",
        config = function()
            require "telescope".setup()
        end,
    }
    use "andymass/vim-matchup"
    use "rafamadriz/friendly-snippets"
    use {
        "hrsh7th/nvim-cmp",
        after = "friendly-snippets",
        config = function()
            require "user.conf.nvim-cmp"
        end,
    }
    use {
        "L3MON4D3/LuaSnip",
        after = "nvim-cmp",
        config = function()
            require "user.conf.luasnip"
        end,

    }
    use {
        "saadparwaiz1/cmp_luasnip",
        after = "LuaSnip",
    }
    use {
        "hrsh7th/cmp-nvim-lua",
        after = "cmp_luasnip",
    }
    use {
        "hrsh7th/cmp-nvim-lsp",
        after = "cmp-nvim-lua",
    }
    use {
        "hrsh7th/cmp-buffer",
        after = "cmp-nvim-lsp",
    }
    use {
        "hrsh7th/cmp-path",
        after = "cmp-buffer",
    }
    use {
        "williamboman/mason.nvim",
        config = function()
            require("mason").setup()
        end,
    }
    use {
        "neovim/nvim-lspconfig",
        after = "mason.nvim",
        config = function()
            require "user.conf.lspconfig"
        end,
    }
    use {
        "ray-x/lsp_signature.nvim",
        after = "nvim-lspconfig",
        config = function()
            require "user.conf.signature"
        end,
    }
    use "sindrets/diffview.nvim"
end)
