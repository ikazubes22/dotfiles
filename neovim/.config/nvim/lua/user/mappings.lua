local M = {}

-- bufferline
vim.keymap.set("n", "<TAB>", "<CMD> BufferLineCycleNext <CR>")
vim.keymap.set("n", "<LEADER>x", require "user.utils".close_buffer)

-- nvimtree
vim.keymap.set("n", "<C-n>", "<CMD> NvimTreeToggle <CR>");
vim.keymap.set("n", "<LEADER>e", "<CMD> NvimTreeFocus <CR>");

-- telescope
vim.keymap.set("n", "<LEADER>ff", "<CMD> Telescope find_files <CR>")
vim.keymap.set("n", "<LEADER>fa", "<CMD> Telescope find_files follow=true no_ignore=true hidden=true <CR>")
vim.keymap.set("n", "<LEADER>fw", "<CMD> Telescope live_grep <CR>")
vim.keymap.set("n", "<LEADER>fb", "<CMD> Telescope buffers <CR>")
vim.keymap.set("n", "<LEADER>fh", "<CMD> Telescope help_tags <CR>")
vim.keymap.set("n", "<LEADER>fo", "<CMD> Telescope oldfiles <CR>")
vim.keymap.set("n", "<LEADER>tk", "<CMD> Telescope keymaps <CR>")
vim.keymap.set("n", "<LEADER>cm", "<CMD> Telescope git_commits <CR>")
vim.keymap.set("n", "<LEADER>gt", "<CMD> Telescope git_status <CR>")

-- lsp
vim.keymap.set("n", "<LEADER>f", function() vim.diagnostic.open_float() end)
vim.keymap.set("n", "[d", function() vim.diagnostic.goto_prev() end)
vim.keymap.set("n", "d]", function() vim.diagnostic.goto_next() end)
vim.keymap.set("n", "<LEADER>q", function() vim.diagnostic.setloclist() end)

-- The following LSP mappings are only applied when a LSP server is attached
M.set_lsp_mappings = function()
    vim.keymap.set("n", "gD", function() vim.lsp.buf.declaration() end)
    vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end)
    vim.keymap.set("n", "gi", function() vim.lsp.buf.implementation() end)
    vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end)
    vim.keymap.set("n", "gs", function() vim.lsp.buf.signature_help() end)
    vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end)
    vim.keymap.set("n", "D", function() vim.lsp.buf.type_definition() end)
    vim.keymap.set("n", "ra", function() vim.lsp.buf.rename() end)
    vim.keymap.set("n", "ca", function() vim.lsp.buf.code_action() end)
    vim.keymap.set("n", "<LEADER>fm", function() vim.lsp.buf.formatting() end)
end

return M
