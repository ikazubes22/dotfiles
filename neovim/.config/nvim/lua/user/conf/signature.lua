local status_ok, signature = pcall(require, "lsp_signature")
if not status_ok then
    return
end

local options = {
    bind = true,
    doc_lines = 20,
    floating_window = true,
    floating_window_above_cur_line = true,
    fix_pos = true,
    hint_enable = true,
    hint_prefix = "🔗 ",
    hint_scheme = "String",
    hi_parameter = "LspSignatureActiveParameter",
    max_height = 22,
    max_width = 120,
    handler_opts = {
        border = "rounded" -- double, rounded, single, shadow, none
    },
}

signature.setup(options)
