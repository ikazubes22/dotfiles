local status_ok, indentbl = pcall(require, "indent_blankline")
if not status_ok then
    return
end

indentbl.setup {
    space_char_blankline = " ",
    show_current_context = true,
    show_current_context_start = true,
}
