vim.g.mapleader = " "

local options = {
    confirm = true, -- dialog for confirmation
    laststatus = 3, -- Always & only last window
    title = true, -- Use window title
    clipboard = "unnamedplus", -- allows neovim to access the system clipboard
    cursorline = true, -- User cursor line

    expandtab = true, -- convert tabs to spaces
    shiftwidth = 4, -- the number of spaces inserted for each indentation
    tabstop = 4, -- insert 2 spaces for a tab
    showtabline = 2, -- always show tabs
    smartindent = true, -- make indenting smarter again

    ignorecase = true, -- ignore case in search patterns
    smartcase = true, -- smart case
    mouse = "a", -- allow the mouse to be used in neovim

    number = true, -- set numbered lines
    numberwidth = 2, -- set number column width to 2 {default 4}
    relativenumber = false, -- set relative numbered lines off

    signcolumn = "yes", -- always show the sign column, otherwise it would shift the text each time
    splitbelow = true, -- force all horizontal splits to go below current window
    splitright = true, -- force all vertical splits to go to the right of current window
    wrap = false, -- Don't auto wrap

    termguicolors = true, -- true colors

    updatetime = 300, -- faster completion (4000ms default)
    timeoutlen = 500, -- time to wait for a mapped sequence to complete (in milliseconds)

    hlsearch = true, -- highlight all matches on previous search pattern
    swapfile = false, -- creates a swapfile
    undofile = true, -- enable persistent undo

    list = true, -- ID certain characters...
    listchars = 'trail:·,precedes:«,extends:»,tab:▸.',
}

for k, v in pairs(options) do
    vim.opt[k] = v
end
