# Dotfile Installation

## Alacritty

### Linux
```shell
stow -t ~ alacritty
```

### Windows
Using powershell run the following commands:
```powershell
New-Item -Path .\alacritty\ -Name windows -ItemType directory
((Get-Content -path .\alacritty\.config\alacritty\alacritty.yml -Raw) -replace 'Hack Nerd Font Mono','Hack NF') | Set-Content -Path .\alacritty\windows\alacritty.yml
New-Item -ItemType SymbolicLink -Path $env:APPDATA\alacritty\alacritty.yml -Target $pwd\alacritty\windows\alacritty.yml
```

## Tmux
```shell
stow -t ~ tmux
```

## NeoVim
### Linux
```shell
stow -t ~ neovim
```

### Windows
```powershell
New-Item -ItemType SymbolicLink -Path $env:LOCALAPPDATA\nvim\lua\custom -Target $pwd\neovim\.config\nvim\lua\custom
```
